import requests
import json
import os

def lambda_handler(event, context):
    webhook_url = os.environ['DISCORD_WEBHOOK_URL']
    repeater_info = event.get('repeater', 'default repeater info')

    message = {
        "content": f"Happy Monday @everyone! There is a GMRS net tonight at 8PM! Make sure your radio is charged and tune in to {repeater_info}\n\nIf you have any questions about our nets or if you would like to see the results, check out the #gmrs-nets channel.\n\u200B\n",
        "embeds": [
            {
                "title": "Seattle GMRS Repeaters",
                "description": "Check out the repeater map to see where they are located.",
                "url": "https://s3.us-west-2.amazonaws.com/seattleemergencyhubs.org-static/reference/SeattleGMRSMap.pdf",
                "color": 0x1e90ff
            },
            {
                "title": "Radio Cheat Sheet",
                "description": "Need help with your BTECH GMRS-V2? Here's a quick guide.",
                "url": "https://drive.google.com/file/d/1nab3aXNFj8kirg8KdG_ZOBlZ1Jm6ZglA/view?usp=sharing",
                "color": 0x34a853
            }
        ]
    }

    headers = {'Content-Type': 'application/json'}
    response = requests.post(webhook_url, data=json.dumps(message), headers=headers)
    return {
        'statusCode': 200,
        'body': json.dumps('Message sent to Discord')
    }

if __name__ == "__main__":
    test_event = {
        "repeater": "Channel X"
    }
    lambda_handler(test_event, None)
