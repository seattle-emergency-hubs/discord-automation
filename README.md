# Seattle Emergency Hubs Discord Automation Scripts

This repository contains a collection of scripts designed to automate various tasks for the Seattle Emergency Hubs Discord server.

## gmrs-net-reminder.py

This script sends a reminder message to the Discord server about the GMRS net event. It uses a Discord webhook to send a message to a specific channel. The message includes the time of the event, a reminder to charge radios, and information about the repeater to tune into. It also includes links to a map of Seattle GMRS Repeaters and a Radio Cheat Sheet.

### Usage

The script is designed to be run as an AWS Lambda function. It requires an environment variable `DISCORD_WEBHOOK_URL` to be set with the URL of the Discord webhook.

A test event can be run by executing the script directly. The test event uses "Channel X" as the repeater information.

```python
python gmrs-net-reminder.py
```

## More Project Ideas

- Discord front-end for hub lookup
- Automatic channel/role assignment?
